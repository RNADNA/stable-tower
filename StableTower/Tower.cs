﻿namespace StableTower;

public class Tower
{
    public int Height;
    private readonly Block CurrentBlock;
    public Tower? SubTower;

    public Tower(int height, Block currentBlock, Tower? subTower = null)
    {
        Height = height;
        CurrentBlock = currentBlock;
        SubTower = subTower;
    }

    public override string ToString()
    {
        return SubTower + "\n" + CurrentBlock;
    }
}