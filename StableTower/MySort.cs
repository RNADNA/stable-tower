﻿namespace StableTower;

public static class MySort
{
    public static void HeapSort(Block[] arr, int n)
    {
        for (var i = n / 2 - 1; i >= 0; i--) //build max-heap
            Heapify(arr, n, i);
        for (var i = n - 1; i >= 0; i--) //sort
        {
            (arr[0], arr[i]) = (arr[i], arr[0]);
            Heapify(arr, i, 0);
        }
    }

    private static void Heapify(Block[] arr, int n, int i)
    {
        while (true)
        {
            var largest = i;
            var left = 2 * i + 1;
            var right = 2 * i + 2;
            if (left < n && arr[left].Length > arr[largest].Length) largest = left;
            if (right < n && arr[right].Length > arr[largest].Length) largest = right;
            if (largest != i)
            {
                (arr[i], arr[largest]) = (arr[largest], arr[i]);
                i = largest;
                continue;
            }

            break;
        }
    }
}