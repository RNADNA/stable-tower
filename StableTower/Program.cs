﻿namespace StableTower
{
    internal static class Program
    {
        #region Main

        private static void Main()
        {
            var runs = new[] {20, 30};
            foreach (var run in runs)
            {
                var towerList = RandomBlocks(run, 1, 200);
                var maxTower = MakeTowerDynamicProgramming(towerList);
                Console.WriteLine($"Maximum Tower Height: {maxTower.Height}");
                Console.WriteLine($"Tower:\n{maxTower}");
            }
        }

        #endregion

        #region Dynamic Programming

        private static Tower MakeTowerDynamicProgramming(Block[] towerList)
        {
            MySort.HeapSort(towerList, towerList.Length);
            var towers = MakeTowerArray(towerList);
            for (var i = 1; i < towerList.Length; i++)
            {
                var stacked = new List<Block>();
                for (var j = 0; j < i; j++)
                {
                    if (CanBeStacked(towerList[j], towerList[i]))
                    {
                        stacked.Add(towerList[j]);
                    }
                }

                var maxBlock = MaximumStackedHeight(stacked);
                if (maxBlock == null) continue;
                towers[towerList[i]].Height += towers[maxBlock].Height;
                towers[towerList[i]].SubTower = towers[maxBlock];
            }

            return MaximumHeight(towers);
        }

        private static Block? MaximumStackedHeight(List<Block> stacked)
        {
            if (stacked.Count == 0)
                return null;

            var maxHeight = stacked[0].Height;
            var maxIndex = 0;
            for (var i = 1; i < stacked.Count; i++)
            {
                if (maxHeight >= stacked[i].Height) continue;
                maxHeight = stacked[i].Height;
                maxIndex = i;
            }

            return stacked[maxIndex];
        }

        private static Tower MaximumHeight(Dictionary<Block, Tower> towers)
        {
            var towerArray = towers.Values.ToArray();
            var maxHeight = towerArray[0].Height;
            var maxIndex = 0;
            for (var i = 1; i < towerArray.Length; i++)
            {
                if (maxHeight >= towerArray[i].Height) continue;
                maxHeight = towerArray[i].Height;
                maxIndex = i;
            }

            return towerArray[maxIndex];
        }

        private static bool CanBeStacked(Block top, Block bottom)
        {
            return top.Length < bottom.Length && top.Width < bottom.Width;
        }

        private static Dictionary<Block, Tower> MakeTowerArray(Block[] towerList)
        {
            return towerList.ToDictionary(block => block, block => new Tower(block.Height, block));
        }

        #endregion

        #region Initialization

        private static Block[] RandomBlocks(int n, int minRange, int maxRange) //o(n)
        {
            var rand = new Random(0);
            var listNumbers = new List<int>[3];
            for (var i = 0; i < listNumbers.Length; i++)
            {
                listNumbers[i] = new List<int>();
                listNumbers[i].AddRange(Enumerable.Range(minRange, maxRange)
                    .OrderBy(_ => rand.Next()).Take(n));
            }

            return ToBlocks(listNumbers[0], listNumbers[1], listNumbers[2]);
        }

        private static Block[] ToBlocks(IReadOnlyList<int> height, IReadOnlyList<int> width,
            IReadOnlyList<int> length) //o(n)
        {
            var blocks = new List<Block>();
            var n = height.Count;
            for (var i = 0; i < n; i++)
            {
                var block = new Block(height[i], width[i], length[i], i);
                blocks.Add(block);
            }

            return blocks.ToArray();
        }

        #endregion
    }
}