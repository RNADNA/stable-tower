﻿namespace StableTower;

public class Block
{
    public readonly int Height;
    public readonly int Width;
    public readonly int Length;
    private readonly int Index;

    public Block(int height, int width, int length, int index)
    {
        Height = height;
        Width = width;
        Length = length;
        Index = index;
    }

    public override string ToString()
    {
        return $"Block {Index}: {Height}x{Width}x{Length}";
    }
}